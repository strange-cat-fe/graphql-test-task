import { injectable } from 'inversify';
import { Connection, createConnection } from 'typeorm';
import { IMongoService } from './interfaces/IMongoService';

import { Game } from '../entities/Game';

@injectable()
export class MongoService implements IMongoService {
  private _connection!: Connection;

  async init() {
    if (!this._connection) {
      const connection = await createConnection({
        type: 'mongodb',
        host: 'localhost',
        port: 27017,
        database: 'graphql-test-task',
        logging: false,
        synchronize: true,
        useUnifiedTopology: true,
        entities: [Game]
      });

      this._connection = connection;
    }
  }

  get connection() {
    return this._connection;
  }
}
