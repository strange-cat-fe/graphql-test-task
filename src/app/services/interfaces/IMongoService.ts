import { Connection } from 'typeorm';

export interface IMongoService {
  connection: Connection;
  init(): Promise<void>;
}
