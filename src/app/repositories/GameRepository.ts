import { injectable } from 'inversify';
import { Game } from '../entities/Game';
import { BaseRepository } from './base/BaseRepository';
import { IGameRepository } from './interfaces/IGameRepository';

@injectable()
export class GameRepository
  extends BaseRepository<Game>
  implements IGameRepository {
  constructor() {
    super(Game);
  }

  async save(game: Game): Promise<Game> {
    game.calculateResult().then(() => {
      this.repository.save(game);
    });

    return this.repository.save(game);
  }

  findById(id: string): Promise<Game | undefined> {
    return this.repository.findOne(id);
  }

  findAll(): Promise<Game[]> {
    return this.repository.find({});
  }
}
