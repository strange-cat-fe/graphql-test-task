import { inject, injectable } from 'inversify';
import { Types } from '../../inversify/Types';
import { Game } from '../entities/Game';
import { IGameRepository } from '../repositories/interfaces/IGameRepository';
import { IGameBusiness } from './interfaces/IGameBusiness';

@injectable()
export class GameBusiness implements IGameBusiness {
  constructor(
    @inject(Types.GameRepository) private _gameRepository: IGameRepository
  ) {}

  async findById(id: string): Promise<Game | undefined> {
    return await this._gameRepository.findById(id);
  }

  async save(game: Game): Promise<Game> {
    return await this._gameRepository.save(game);
  }

  async findAll(): Promise<Game[]> {
    return await this._gameRepository.findAll();
  }
}
