import { Redis } from 'ioredis';
import connect from 'connect-redis';
import session from 'express-session';

export const connectRedisSession = (redis: Redis) => {
  const RedisStore = connect(session);

  return session({
    name: 'gameSession',
    store: new RedisStore({
      client: redis,
      disableTouch: true
    }),
    cookie: {
      maxAge: 1000 * 60 * 60 * 24 * 365, // one year
      httpOnly: true,
      sameSite: 'lax',
      secure: false // TODO in prod set true
    },
    saveUninitialized: false,
    secret: 'afl;uhg-aj323jfjHKJjOIF89la9_',
    resave: false
  });
};
