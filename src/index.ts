import 'reflect-metadata';
import Redis from 'ioredis';
import express from 'express';
import { IMongoService } from './app/services/interfaces/IMongoService';
import { connectRedisSession } from './utils/middlewares/connectRedis';
import { PlayerResolver } from './app/resolvers/PlayerResolver';
import { GameResolver } from './app/resolvers/GameResolver';
import { ApolloServer } from 'apollo-server-express';
import { MyContext } from './utils/types/MyContext';
import { container } from './inversify/container';
import { buildSchema } from 'type-graphql';
import { Types } from './inversify/Types';

async function main(): Promise<void> {
  await container.get<IMongoService>(Types.MongoService).init();

  const redis = new Redis();

  const app = express();

  app.use(connectRedisSession(redis));

  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [GameResolver, PlayerResolver],
      validate: false
    }),

    context: ({ req, res }): MyContext => ({ req, res, redis })
  });

  apolloServer.applyMiddleware({
    app,
    path: '/',
    cors: true
  });

  app.listen(5000, () => console.log('Server has started...'));
}

main();
