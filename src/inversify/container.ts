import { Container } from 'inversify';
import { GameBusiness } from '../app/business/GameBusiness';
import { GameRepository } from '../app/repositories/GameRepository';
import { IMongoService } from '../app/services/interfaces/IMongoService';
import { MongoService } from '../app/services/MongoService';
import { Types } from './Types';

const container = new Container();

container.bind(Types.GameBusiness).to(GameBusiness).inSingletonScope();
container.bind(Types.GameRepository).to(GameRepository).inSingletonScope();

container.bind(Types.MongoService).to(MongoService).inSingletonScope();

container
  .bind(Types.MongoProvider)
  .toFunction(
    () => container.get<IMongoService>(Types.MongoService).connection
  );

export { container };
